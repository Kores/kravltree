use std::fmt::{Debug, Display};
use std::ptr::NonNull;

const SUCC_MASK: i32 = (1 << 31) as i32;
const PRED_MASK: i32 = 1 << 30;
const BALANCE_MASK: i32 = 0xFF;

/// A wrapper around unsafe entry pointer, all operations are unsafe but the functions are exposed
/// as safe operations (this may change in the future).
///
/// This pointer is safe to use as long as the implementation is correctly made.
pub(crate) struct EntryPtr<K, V> {
    pub ptr: NonNull<Entry<K, V>>,
}

impl<K, V> Clone for EntryPtr<K, V> {
    fn clone(&self) -> Self {
        Self { ptr: self.ptr }
    }
}

impl<K, V> Copy for EntryPtr<K, V> {}

impl<K, V> EntryPtr<K, V> {
    pub fn new(ptr: NonNull<Entry<K, V>>) -> Self {
        EntryPtr { ptr }
    }

    pub fn from_box(box_: Box<Entry<K, V>>) -> Self {
        Self::new(NonNull::new(Box::into_raw(box_)).unwrap())
    }

    pub fn info(&self) -> i32 {
        unsafe { self.ptr.as_ref().info }
    }

    pub fn left_field(&self) -> Option<EntryPtr<K, V>> {
        unsafe { self.ptr.as_ref().left }
    }

    pub fn left_field_as_ptr(&self) -> Option<*mut Entry<K, V>> {
        unsafe { self.ptr.as_ref().left }.map(|p| p.as_ptr())
    }

    pub fn right_field(&self) -> Option<EntryPtr<K, V>> {
        unsafe { self.ptr.as_ref().right }
    }

    pub fn right_field_as_ptr(&self) -> Option<*mut Entry<K, V>> {
        unsafe { self.ptr.as_ref().right }.map(|p| p.as_ptr())
    }

    pub fn raw_left(&self) -> Option<EntryPtr<K, V>> {
        unsafe { self.ptr.as_ref().left_ptr() }
    }

    pub fn raw_right(&self) -> Option<EntryPtr<K, V>> {
        unsafe { self.ptr.as_ref().right_ptr() }
    }

    pub fn pred(&self) -> bool {
        unsafe { self.ptr.as_ref().pred() }
    }

    pub fn succ(&self) -> bool {
        unsafe { self.ptr.as_ref().succ() }
    }

    pub fn left_field_ref(&self) -> Option<&Entry<K, V>> {
        return self.left_field().map(|x| unsafe { x.ptr.as_ref() });
    }

    pub fn right_field_ref(&self) -> Option<&Entry<K, V>> {
        return self.right_field().map(|x| unsafe { x.ptr.as_ref() });
    }

    pub fn balance(&self) -> i8 {
        unsafe { self.ptr.as_ref() }.balance()
    }

    pub fn key(&self) -> &K {
        unsafe { self.ptr.as_ref() }.key()
    }

    pub fn as_mut(&mut self) -> (&K, &mut V) {
        unsafe { self.ptr.as_mut() }.as_mut()
    }

    // Setters

    pub fn set_pred(&mut self, pred: bool) {
        unsafe { self.ptr.as_mut() }.set_pred(pred)
    }

    pub fn set_succ(&mut self, succ: bool) {
        unsafe { self.ptr.as_mut() }.set_succ(succ)
    }

    pub fn set_pred_entry(&mut self, pred: Option<EntryPtr<K, V>>) {
        unsafe { self.ptr.as_mut() }.set_pred_entry(pred)
    }

    pub fn set_succ_entry(&mut self, succ: Option<EntryPtr<K, V>>) {
        unsafe { self.ptr.as_mut() }.set_succ_entry(succ)
    }

    pub fn set_left_entry(&mut self, left: Option<EntryPtr<K, V>>) {
        unsafe { self.ptr.as_mut() }.set_left_entry(left)
    }

    pub(crate) fn set_left_field(&mut self, left: Option<EntryPtr<K, V>>) {
        unsafe { self.ptr.as_mut().set_left_field(left) }
    }

    pub fn set_right_entry(&mut self, right: Option<EntryPtr<K, V>>) {
        unsafe { self.ptr.as_mut() }.set_right_entry(right)
    }

    pub(crate) fn set_right_field(&mut self, right: Option<EntryPtr<K, V>>) {
        unsafe { self.ptr.as_mut().set_right_field(right) }
    }

    pub fn set_balance(&mut self, balance: i8) {
        unsafe { self.ptr.as_mut() }.set_balance(balance)
    }

    pub fn increment_balance(&mut self) {
        unsafe { self.ptr.as_mut() }.increment_balance()
    }

    pub fn decrement_balance(&mut self) {
        unsafe { self.ptr.as_mut() }.decrement_balance()
    }

    pub fn next_as_ptr(&self) -> Option<EntryPtr<K, V>> {
        unsafe { self.ptr.as_ref() }.next_as_ptr()
    }

    pub fn prev_as_ptr(&self) -> Option<EntryPtr<K, V>> {
        unsafe { self.ptr.as_ref() }.prev_as_ptr()
    }

    pub fn set_value(&mut self, value: V) -> V {
        unsafe { self.ptr.as_mut() }.set_value(value)
    }

    pub fn as_ptr(&self) -> *mut Entry<K, V> {
        self.ptr.as_ptr()
    }

    pub fn take_entry(self) -> Entry<K, V> {
        *unsafe { Box::from_raw(self.ptr.as_ptr()) }
    }
}

enum EntryValue<V> {
    Occupied(V),
    Empty,
}

impl<V> EntryValue<V> {
    fn new(value: V) -> Self {
        EntryValue::Occupied(value)
    }

    fn take(&mut self) -> V {
        match self {
            EntryValue::Empty => panic!("trying to take from empty entry: value already moved."),
            _ => {}
        }

        let v = std::mem::replace(self, EntryValue::Empty);
        match v {
            EntryValue::Occupied(v) => v,
            EntryValue::Empty => unreachable!(),
        }
    }

    fn get(&self) -> &V {
        match self {
            EntryValue::Occupied(v) => v,
            EntryValue::Empty => panic!("trying to get from empty entry: value already moved."),
        }
    }

    fn get_mut(&mut self) -> &mut V {
        match self {
            EntryValue::Occupied(v) => v,
            EntryValue::Empty => panic!("trying to get from empty entry: value already moved."),
        }
    }
}

impl<V> Clone for EntryValue<V>
where
    V: Clone,
{
    fn clone(&self) -> Self {
        Self::Occupied(self.get().clone())
    }
}

impl<V> PartialEq for EntryValue<V>
where
    V: PartialEq,
{
    fn eq(&self, other: &Self) -> bool {
        self.get() == other.get()
    }
}

pub(crate) struct Entry<K, V> {
    key: EntryValue<K>,
    value: EntryValue<V>,
    left: Option<EntryPtr<K, V>>,
    right: Option<EntryPtr<K, V>>,
    info: i32,
}

impl<K, V> Entry<K, V> {
    pub fn new(key: K, value: V) -> Self {
        Self {
            key: EntryValue::new(key),
            value: EntryValue::new(value), /*unsafe { NonNull::new(Box::into_raw(Box::new(value))) }.unwrap()*/
            left: None,
            right: None,
            info: SUCC_MASK | PRED_MASK,
        }
    }

    pub fn key(&self) -> &K {
        self.key.get()
    }

    pub fn take_key(&mut self) -> K {
        self.key.take()
    }

    pub fn value(&self) -> &V {
        self.value.get()
    }

    pub fn as_mut(&mut self) -> (&K, &mut V) {
        (self.key.get(), self.value.get_mut())
    }

    #[inline]
    pub fn take(mut self) -> V {
        self.value.take()
    }

    pub fn left_ptr(&self) -> Option<EntryPtr<K, V>> {
        if (self.info & PRED_MASK) != 0 {
            return None;
        } else {
            return self.left;
        }
    }

    pub fn left(&self) -> Option<&Entry<K, V>> {
        if (self.info & PRED_MASK) != 0 {
            return None;
        } else {
            return self.left.map(|x| unsafe { x.ptr.as_ref() });
        }
    }

    pub fn right_ptr(&self) -> Option<EntryPtr<K, V>> {
        if (self.info & SUCC_MASK) != 0 {
            return None;
        } else {
            return self.right;
        }
    }

    pub fn right(&self) -> Option<&Entry<K, V>> {
        if (self.info & SUCC_MASK) != 0 {
            return None;
        } else {
            return self.right.map(|x| unsafe { x.ptr.as_ref() });
        }
    }

    pub fn pred(&self) -> bool {
        (self.info & PRED_MASK) != 0
    }

    pub fn succ(&self) -> bool {
        (self.info & SUCC_MASK) != 0
    }

    pub fn set_pred(&mut self, pred: bool) {
        if pred {
            self.info |= PRED_MASK;
        } else {
            self.info &= !PRED_MASK;
        }
    }

    pub fn set_succ(&mut self, succ: bool) {
        if succ {
            self.info |= SUCC_MASK;
        } else {
            self.info &= !SUCC_MASK;
        }
    }

    pub fn set_pred_entry(&mut self, pred: Option<EntryPtr<K, V>>) {
        if let Some(pred) = pred {
            self.set_pred(true);
            self.left = Some(pred);
        } else {
            self.set_pred(false);
            self.left = None;
        }
    }

    pub fn set_succ_entry(&mut self, succ: Option<EntryPtr<K, V>>) {
        if let Some(succ) = succ {
            self.set_succ(true);
            self.right = Some(succ);
        } else {
            self.set_succ(false);
            self.right = None;
        }
    }

    pub fn set_left_entry(&mut self, left: Option<EntryPtr<K, V>>) {
        if let Some(left) = left {
            self.left = Some(left);
            self.info &= !PRED_MASK;
        } else {
            self.left = None;
        }
    }

    pub(crate) unsafe fn set_left_field(&mut self, left: Option<EntryPtr<K, V>>) {
        self.left = left;
    }

    pub fn set_right_entry(&mut self, right: Option<EntryPtr<K, V>>) {
        if let Some(right) = right {
            self.right = Some(right);
            self.info &= !SUCC_MASK;
        } else {
            self.right = None;
        }
    }

    pub(crate) unsafe fn set_right_field(&mut self, right: Option<EntryPtr<K, V>>) {
        self.right = right;
    }

    pub fn balance(&self) -> i8 {
        self.info as i8
    }

    pub fn set_balance(&mut self, balance: i8) {
        self.info &= !BALANCE_MASK;
        self.info |= balance as i32 & BALANCE_MASK;
    }

    pub fn increment_balance(&mut self) {
        self.info =
            self.info & !BALANCE_MASK | ((self.info as i8 + 1) as i32 & BALANCE_MASK) as i32;
    }

    pub fn decrement_balance(&mut self) {
        self.info =
            self.info & !BALANCE_MASK | ((self.info as i8 - 1) as i32 & BALANCE_MASK) as i32;
    }

    pub fn next_as_ptr(&self) -> Option<EntryPtr<K, V>> {
        let mut next = self.right;
        if (self.info & SUCC_MASK) == 0 {
            while let Some(ref mut x) = next {
                if (x.info() & PRED_MASK) == 0 {
                    next = x.left_field();
                } else {
                    break;
                }
            }
        }
        next
    }

    pub fn prev_as_ptr(&self) -> Option<EntryPtr<K, V>> {
        let mut prev = self.left;
        if (self.info & PRED_MASK) == 0 {
            while let Some(ref mut x) = prev {
                if (x.info() & SUCC_MASK) == 0 {
                    prev = x.right_field();
                } else {
                    break;
                }
            }
        }
        prev
    }

    pub fn set_value(&mut self, value: V) -> V {
        std::mem::replace(&mut self.value, EntryValue::new(value)).take()
    }

    pub fn set_key(&mut self, key: K) -> K {
        std::mem::replace(&mut self.key, EntryValue::new(key)).take()
    }
}

impl<K, V> Clone for Entry<K, V>
where
    K: Clone,
    V: Clone,
{
    fn clone(&self) -> Self {
        Self {
            key: self.key.clone(),
            value: self.value.clone(),
            left: self.left.clone(),
            right: self.right.clone(),
            info: self.info,
        }
    }
}

impl<K, V> Drop for Entry<K, V> {
    fn drop(&mut self) {
        // TODO: Remove reference to the entry from the tree right in the rotation logic.

        // NOTE: A reference to the same pointer may exist at some point at the time after a rotation,
        // this function only drops fields that this [`crate::entry::Entry`] owns, by default, entries
        // have PRED_MASK and SUCC_MASK set at the same time, which means that this entry
        // owns both the `left` and `right` side. Those bits are removed after a rotation signaling
        // that this entry owns either `left` or `right` side, not both. The removal logic MUST
        // clear the `left` and `right` fields to ensure that there is no dangling pointers, since
        // this field may hold a pointer to another field without having ownership to it.
        // This happens because this implementation of AVL Tree relies on these values for some operations,
        // also, the drop of `left` or `right` value MUST happen only AFTER the cleanup has been done
        // to avoid access to dangling pointers.
        if let Some(v) = self.right_ptr().take() {
            let _ = unsafe { Box::from_raw(v.as_ptr()) };
        }
        if let Some(v) = self.left_ptr().take() {
            let _ = unsafe { Box::from_raw(v.as_ptr()) };
        }
    }
}

impl<K, V> Display for Entry<K, V>
where
    K: Display,
    V: Display,
{
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{} => {}", self.key(), self.value())
    }
}

impl<K, V> Debug for Entry<K, V>
where
    K: Debug,
    V: Debug,
{
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:?} => {:?}", self.key(), self.value())
    }
}

impl<K, V> PartialOrd for Entry<K, V>
where
    K: PartialOrd + PartialEq,
    V: PartialEq,
{
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.key().partial_cmp(other.key())
    }
}

impl<K, V> PartialEq for Entry<K, V>
where
    K: PartialEq,
    V: PartialEq,
{
    fn eq(&self, other: &Self) -> bool {
        self.key == other.key && self.value == other.value
    }
}

impl<K, V> Eq for Entry<K, V>
where
    K: PartialEq + Eq,
    V: PartialEq,
{
}

impl<K, V> Ord for Entry<K, V>
where
    K: PartialOrd + Ord,
    V: PartialOrd,
{
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.key().cmp(other.key())
    }
}
