pub mod avltree;
mod entry;
pub mod avlset;

#[cfg(test)]
mod tests {
    use crate::avlset::AvlTreeSet;
    use crate::avltree;

    #[test]
    fn insertion_test() {
        let mut a = avltree::AvlTreeMap::new();
        a.insert(3, 1);
        a.insert(-1, 1);
        a.insert(2, 1);
        a.insert(1, 1);
        assert_eq!(4, a.len());
        if cfg!(feature = "print-tree") {
            assert_eq!(
                "AVLTree {
    Root(2 => 1)
    ├── L(-1 => 1)
    │   └── R(1 => 1)
    └── R(3 => 1)
}",
                format!("{:?}", a)
            );
        }
    }

    #[test]
    #[cfg(feature = "print-tree")]
    fn proper_structure_after_insertion() {
        let mut a = avltree::AvlTreeMap::new();
        a.insert(3, 1);
        a.insert(-1, 1);
        a.insert(2, 1);
        a.insert(7, 1);
        a.insert(1, 1);
        a.insert(42, 1);
        a.insert(-50, 1);
        assert_eq!(7, a.len());
        assert_eq!(
            "AVLTree {
    Root(2 => 1)
    ├── L(-1 => 1)
    │   ├── L(-50 => 1)
    │   └── R(1 => 1)
    └── R(7 => 1)
        ├── L(3 => 1)
        └── R(42 => 1)
}",
            format!("{:?}", a)
        );
    }

    #[test]
    fn proper_structure_after_value_change() {
        let mut a = avltree::AvlTreeMap::new();
        a.insert(3, 1);
        a.insert(-1, 1);
        a.insert(2, 1);
        a.insert(7, 1);
        a.insert(1, 1);
        a.insert(42, 1);
        a.insert(-50, 1);
        a.insert(-40, 1);
        assert_eq!(8, a.len());
        if cfg!(feature = "print-tree") {
            assert_eq!(
                "AVLTree {
    Root(2 => 1)
    ├── L(-1 => 1)
    │   ├── L(-50 => 1)
    │   │   └── R(-40 => 1)
    │   └── R(1 => 1)
    └── R(7 => 1)
        ├── L(3 => 1)
        └── R(42 => 1)
}",
                format!("{:?}", a)
            );
        }
        assert_eq!(Some(1), a.insert(-40, 20));
        assert_eq!(8, a.len());
        if cfg!(feature = "print-tree") {
            assert_eq!(
                "AVLTree {
    Root(2 => 1)
    ├── L(-1 => 1)
    │   ├── L(-50 => 1)
    │   │   └── R(-40 => 20)
    │   └── R(1 => 1)
    └── R(7 => 1)
        ├── L(3 => 1)
        └── R(42 => 1)
}",
                format!("{:?}", a)
            )
        }
    }

    #[test]
    fn proper_structure_after_removal() {
        let mut a = avltree::AvlTreeMap::new();
        a.insert(3, 1);
        a.insert(-1, 1);
        a.insert(2, 1);
        a.insert(7, 1);
        a.insert(1, 1);
        a.insert(42, 1);
        a.insert(-50, 1);
        a.insert(-40, 1);
        assert_eq!(8, a.len());
        if cfg!(feature = "print-tree") {
            assert_eq!(
                "AVLTree {
    Root(2 => 1)
    ├── L(-1 => 1)
    │   ├── L(-50 => 1)
    │   │   └── R(-40 => 1)
    │   └── R(1 => 1)
    └── R(7 => 1)
        ├── L(3 => 1)
        └── R(42 => 1)
}",
                format!("{:?}", a)
            );
        }
        assert_eq!(Some(1), a.insert(-40, 20));
        assert_eq!(8, a.len());
        if cfg!(feature = "print-tree") {
            assert_eq!(
                "AVLTree {
    Root(2 => 1)
    ├── L(-1 => 1)
    │   ├── L(-50 => 1)
    │   │   └── R(-40 => 20)
    │   └── R(1 => 1)
    └── R(7 => 1)
        ├── L(3 => 1)
        └── R(42 => 1)
}",
                format!("{:?}", a)
            );
        }

        assert_eq!(Some(1), a.remove(&-50));
        assert_eq!(7, a.len());
        if cfg!(feature = "print-tree") {
            assert_eq!(
                "AVLTree {
    Root(2 => 1)
    ├── L(-1 => 1)
    │   ├── L(-40 => 20)
    │   └── R(1 => 1)
    └── R(7 => 1)
        ├── L(3 => 1)
        └── R(42 => 1)
}",
                format!("{:?}", a)
            )
        }
    }

    #[test]
    fn proper_structure_after_removal_plus_iteration() {
        let mut a = avltree::AvlTreeMap::new();
        a.insert(3, 1);
        a.insert(-1, 1);
        a.insert(2, 1);
        a.insert(7, 1);
        a.insert(1, 1);
        a.insert(42, 1);
        a.insert(-50, 1);
        a.insert(-40, 1);
        assert_eq!(8, a.len());
        if cfg!(feature = "print-tree") {
            assert_eq!(
                "AVLTree {
    Root(2 => 1)
    ├── L(-1 => 1)
    │   ├── L(-50 => 1)
    │   │   └── R(-40 => 1)
    │   └── R(1 => 1)
    └── R(7 => 1)
        ├── L(3 => 1)
        └── R(42 => 1)
}",
                format!("{:?}", a)
            );
        }
        assert_eq!(Some(1), a.insert(-40, 20));
        if cfg!(feature = "print-tree") {
            assert_eq!(
                "AVLTree {
    Root(2 => 1)
    ├── L(-1 => 1)
    │   ├── L(-50 => 1)
    │   │   └── R(-40 => 20)
    │   └── R(1 => 1)
    └── R(7 => 1)
        ├── L(3 => 1)
        └── R(42 => 1)
}",
                format!("{:?}", a)
            );
        }

        assert_eq!(Some(1), a.remove(&-50));
        if cfg!(feature = "print-tree") {
            assert_eq!(
                "AVLTree {
    Root(2 => 1)
    ├── L(-1 => 1)
    │   ├── L(-40 => 20)
    │   └── R(1 => 1)
    └── R(7 => 1)
        ├── L(3 => 1)
        └── R(42 => 1)
}",
                format!("{:?}", a)
            );
        }

        let mut iter = a.iter();

        assert_eq!(Some((&-40, &20)), iter.next());
        assert_eq!(Some((&-1, &1)), iter.next());
        assert_eq!(Some((&1, &1)), iter.next());
        assert_eq!(Some((&2, &1)), iter.next());
        assert_eq!(Some((&3, &1)), iter.next());
        assert_eq!(Some((&7, &1)), iter.next());
        assert_eq!(Some((&42, &1)), iter.next());
        assert_eq!(None, iter.next());
        assert_eq!(None, iter.next());

        let mut iter2 = a.iter();

        assert_eq!(Some((&42, &1)), iter2.next_back());
        assert_eq!(Some((&7, &1)), iter2.next_back());
        assert_eq!(Some((&3, &1)), iter2.next_back());
        assert_eq!(Some((&2, &1)), iter2.next_back());
        assert_eq!(Some((&1, &1)), iter2.next_back());
        assert_eq!(Some((&-1, &1)), iter2.next_back());
        assert_eq!(Some((&-40, &20)), iter2.next_back());
        assert_eq!(None, iter2.next_back());
        assert_eq!(None, iter2.next_back());

        let mut iter3 = a.iter();

        assert_eq!(Some((&-40, &20)), iter3.next());
        assert_eq!(Some((&42, &1)), iter3.next_back());
        assert_eq!(Some((&-1, &1)), iter3.next());
        assert_eq!(Some((&7, &1)), iter3.next_back());
        assert_eq!(Some((&1, &1)), iter3.next());
        assert_eq!(Some((&3, &1)), iter3.next_back());
        assert_eq!(Some((&2, &1)), iter3.next());
        assert_eq!(None, iter3.next_back());
        assert_eq!(None, iter3.next());
    }

    #[test]
    fn proper_structure_after_clear() {
        let mut a = avltree::AvlTreeMap::new();
        a.insert(3, 1);
        a.insert(-1, 1);
        a.insert(2, 1);
        a.insert(7, 1);
        a.insert(1, 1);
        a.insert(42, 1);
        a.insert(-50, 1);
        a.insert(-40, 1);
        assert_eq!(8, a.len());
        if cfg!(feature = "print-tree") {
            assert_eq!(
                "AVLTree {
    Root(2 => 1)
    ├── L(-1 => 1)
    │   ├── L(-50 => 1)
    │   │   └── R(-40 => 1)
    │   └── R(1 => 1)
    └── R(7 => 1)
        ├── L(3 => 1)
        └── R(42 => 1)
}",
                format!("{:?}", a)
            );
        }
        assert_eq!(Some(1), a.insert(-40, 20));
        if cfg!(feature = "print-tree") {
            assert_eq!(
                "AVLTree {
    Root(2 => 1)
    ├── L(-1 => 1)
    │   ├── L(-50 => 1)
    │   │   └── R(-40 => 20)
    │   └── R(1 => 1)
    └── R(7 => 1)
        ├── L(3 => 1)
        └── R(42 => 1)
}",
                format!("{:?}", a)
            );
        }

        assert_eq!(Some(1), a.remove(&-50));
        if cfg!(feature = "print-tree") {
            assert_eq!(
                "AVLTree {
    Root(2 => 1)
    ├── L(-1 => 1)
    │   ├── L(-40 => 20)
    │   └── R(1 => 1)
    └── R(7 => 1)
        ├── L(3 => 1)
        └── R(42 => 1)
}",
                format!("{:?}", a)
            );
        }
        let mut iter = a.iter();

        assert_eq!(Some((&-40, &20)), iter.next());
        assert_eq!(Some((&-1, &1)), iter.next());
        assert_eq!(Some((&1, &1)), iter.next());
        assert_eq!(Some((&2, &1)), iter.next());
        assert_eq!(Some((&3, &1)), iter.next());
        assert_eq!(Some((&7, &1)), iter.next());
        assert_eq!(Some((&42, &1)), iter.next());
        assert_eq!(None, iter.next());
        assert_eq!(None, iter.next());

        let mut iter2 = a.iter();

        assert_eq!(Some((&42, &1)), iter2.next_back());
        assert_eq!(Some((&7, &1)), iter2.next_back());
        assert_eq!(Some((&3, &1)), iter2.next_back());
        assert_eq!(Some((&2, &1)), iter2.next_back());
        assert_eq!(Some((&1, &1)), iter2.next_back());
        assert_eq!(Some((&-1, &1)), iter2.next_back());
        assert_eq!(Some((&-40, &20)), iter2.next_back());
        assert_eq!(None, iter2.next_back());
        assert_eq!(None, iter2.next_back());

        let mut iter3 = a.iter();

        assert_eq!(Some((&-40, &20)), iter3.next());
        assert_eq!(Some((&42, &1)), iter3.next_back());
        assert_eq!(Some((&-1, &1)), iter3.next());
        assert_eq!(Some((&7, &1)), iter3.next_back());
        assert_eq!(Some((&1, &1)), iter3.next());
        assert_eq!(Some((&3, &1)), iter3.next_back());
        assert_eq!(Some((&2, &1)), iter3.next());
        assert_eq!(None, iter3.next_back());
        assert_eq!(None, iter3.next());

        a.clear();

        if cfg!(feature = "print-tree") {
            assert_eq!(
                "AVLTree {
}",
                format!("{:?}", a)
            );
        }
    }

    #[test]
    fn proper_order_after_add_to_set() {
        let mut set = AvlTreeSet::new();
        set.insert(9);
        set.insert(7);
        set.insert(10);
        let mut iter = set.iter();
        assert_eq!(Some(&7), iter.next());
        assert_eq!(Some(&10), iter.next_back());
        assert_eq!(Some(&9), iter.next());
        assert_eq!(None, iter.next());
        assert_eq!(None, iter.next_back());
    }

    #[test]
    fn proper_structure_after_retain() {
        let mut a = avltree::AvlTreeMap::new();
        a.insert(3, 1);
        a.insert(-1, 1);
        a.insert(2, 1);
        a.insert(7, 1);
        a.insert(1, 1);
        a.insert(42, 1);
        a.insert(-50, 1);
        a.insert(-40, 1);
        assert_eq!(8, a.len());
        if cfg!(feature = "print-tree") {
            assert_eq!(
                "AVLTree {
    Root(2 => 1)
    ├── L(-1 => 1)
    │   ├── L(-50 => 1)
    │   │   └── R(-40 => 1)
    │   └── R(1 => 1)
    └── R(7 => 1)
        ├── L(3 => 1)
        └── R(42 => 1)
}",
                format!("{:?}", a)
            );
        }
        assert_eq!(Some(1), a.insert(-40, 20));
        if cfg!(feature = "print-tree") {
            assert_eq!(
                "AVLTree {
    Root(2 => 1)
    ├── L(-1 => 1)
    │   ├── L(-50 => 1)
    │   │   └── R(-40 => 20)
    │   └── R(1 => 1)
    └── R(7 => 1)
        ├── L(3 => 1)
        └── R(42 => 1)
}",
                format!("{:?}", a)
            );
        }

        assert_eq!(Some(1), a.remove(&-50));
        if cfg!(feature = "print-tree") {
            assert_eq!(
                "AVLTree {
    Root(2 => 1)
    ├── L(-1 => 1)
    │   ├── L(-40 => 20)
    │   └── R(1 => 1)
    └── R(7 => 1)
        ├── L(3 => 1)
        └── R(42 => 1)
}",
                format!("{:?}", a)
            );
        }
        let mut iter = a.iter();

        assert_eq!(Some((&-40, &20)), iter.next());
        assert_eq!(Some((&-1, &1)), iter.next());
        assert_eq!(Some((&1, &1)), iter.next());
        assert_eq!(Some((&2, &1)), iter.next());
        assert_eq!(Some((&3, &1)), iter.next());
        assert_eq!(Some((&7, &1)), iter.next());
        assert_eq!(Some((&42, &1)), iter.next());
        assert_eq!(None, iter.next());
        assert_eq!(None, iter.next());

        let mut iter2 = a.iter();

        assert_eq!(Some((&42, &1)), iter2.next_back());
        assert_eq!(Some((&7, &1)), iter2.next_back());
        assert_eq!(Some((&3, &1)), iter2.next_back());
        assert_eq!(Some((&2, &1)), iter2.next_back());
        assert_eq!(Some((&1, &1)), iter2.next_back());
        assert_eq!(Some((&-1, &1)), iter2.next_back());
        assert_eq!(Some((&-40, &20)), iter2.next_back());
        assert_eq!(None, iter2.next_back());
        assert_eq!(None, iter2.next_back());

        let mut iter3 = a.iter();

        assert_eq!(Some((&-40, &20)), iter3.next());
        assert_eq!(Some((&42, &1)), iter3.next_back());
        assert_eq!(Some((&-1, &1)), iter3.next());
        assert_eq!(Some((&7, &1)), iter3.next_back());
        assert_eq!(Some((&1, &1)), iter3.next());
        assert_eq!(Some((&3, &1)), iter3.next_back());
        assert_eq!(Some((&2, &1)), iter3.next());
        assert_eq!(None, iter3.next_back());
        assert_eq!(None, iter3.next());

        a.retain(|k, v| {
            if k == &-40 {
                *v = 50;
                true
            } else if k == &3 {
                false
            } else {
                true
            }
        });

        if cfg!(feature = "print-tree") {
            assert_eq!(
                "AVLTree {
    Root(2 => 1)
    ├── L(-1 => 1)
    │   ├── L(-40 => 50)
    │   └── R(1 => 1)
    └── R(7 => 1)
        └── R(42 => 1)
}",
                format!("{:?}", a)
            );
        }
    }
}
