use std::borrow::Borrow;
use std::marker::PhantomData;
use crate::avltree::{AvlTreeIter, AvlTreeMap};

pub struct AvlTreeSet<T> {
    map: AvlTreeMap<T, ()>
}

impl<T> AvlTreeSet<T> {
    pub fn new() -> Self {
        Self {
            map: AvlTreeMap::new()
        }
    }

    pub fn first_value(&self) -> Option<&T> {
        self.map.first_key()
    }

    pub fn last_value(&self) -> Option<&T> {
        self.map.last_key()
    }

    pub fn len(&self) -> usize {
        self.map.len()
    }

    pub fn clear(&mut self) {
        self.map.clear()
    }
}

impl<T> AvlTreeSet<T> where T: PartialOrd + Ord {
    pub fn contains<Q: ?Sized>(&self, value: &Q) -> bool where T: Borrow<Q>, Q: PartialOrd {
        self.map.contains_key(value)
    }

    pub fn get<Q: ?Sized>(&self, value: &Q) -> Option<&T> where T: Borrow<Q>, Q: PartialOrd {
        self.map.find_key(value).map(|v| v.key())
    }

    pub fn insert(&mut self, value: T) -> bool {
        self.map.insert(value, ()).is_none()
    }

    pub fn replace(&mut self, value: T) -> Option<T> {
        self.map.find_entry(&value).map(|e| e.set_key(value))
    }

    pub fn remove<Q: ?Sized>(&mut self, value: &Q) -> bool where T: Borrow<Q>, Q: PartialOrd + Ord {
        self.map.remove(value).is_some()
    }

    pub fn take<Q: ?Sized>(&mut self, value: &Q) -> Option<T> where T: Borrow<Q>, Q: PartialOrd + Ord {
        self.map.remove_key(value).map(|mut e| e.take_key())
    }

    pub fn retain<F>(&mut self, mut f: F)
        where
            F: FnMut(&T) -> bool,
    {
        self.map.retain(|k, _| f(k))
    }

    pub fn iter(&self) -> AvlTreeSetIter<'_, T> {
        AvlTreeSetIter::new(self.map.iter())
    }

}

pub struct AvlTreeSetIter<'a, T> {
    inner: AvlTreeIter<'a, T, ()>,
    _marker: PhantomData<(&'a T, &'a ())>
}

impl<'a, T> AvlTreeSetIter<'a, T> {
    fn new(inner: AvlTreeIter<'a, T, ()>) -> Self {
        Self {
            inner,
            _marker: PhantomData
        }
    }
}

impl<'a, T> Iterator for AvlTreeSetIter<'a, T> where T: PartialOrd {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next().map(|(c, _)| c)
    }
}

impl<'a, T> DoubleEndedIterator for AvlTreeSetIter<'a, T> where T: PartialOrd {
    fn next_back(&mut self) -> Option<Self::Item> {
        self.inner.next_back().map(|(c, _)| c)
    }
}