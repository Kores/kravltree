use std::borrow::Borrow;
use std::marker::PhantomData;
use std::{
    cmp::Ordering,
    fmt::{Debug, Formatter},
    ptr::NonNull,
};

use crate::entry::*;

pub struct AvlTreeMap<K, V> {
    tree: Option<EntryPtr<K, V>>,
    count: usize,
    first: Option<EntryPtr<K, V>>, // Points to an entry inside the Tree
    last: Option<EntryPtr<K, V>>,  // Points to an entry inside the Tree
    modified: bool,                // TODO: We really need this?
    insert_path: [bool; 48],
}

impl<K, V> AvlTreeMap<K, V> {
    pub fn new() -> Self {
        Self {
            tree: None,
            count: 0,
            first: None,
            last: None,
            modified: false,
            insert_path: [bool::default(); 48],
        }
    }

    pub fn len(&self) -> usize {
        self.count
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }


    pub fn first_key(&self) -> Option<&K> {
        if let Some(t) = self.first {
            let k = unsafe { (*t.ptr.as_ptr()).key() };
            Some(k)
        } else {
            None
        }
    }

    pub fn last_key(&self) -> Option<&K> {
        if let Some(t) = self.last {
            let k = unsafe { (*t.ptr.as_ptr()).key() };
            Some(k)
        } else {
            None
        }
    }

    pub fn clear(&mut self) {
        self.count = 0;
        // Drop tree
        let mut start = self.tree;
        if let Some(start) = start.take() {
            let _ = unsafe { Box::from_raw(start.as_ptr()) };
        }
        // /Drop tree
        self.tree = None;
        self.first = None;
        self.last = None;
    }
}

impl<K, V> Clone for AvlTreeMap<K, V> where K: Clone + Ord, V: Clone {
    fn clone(&self) -> Self {
        // Clone here just creates a new AvlTreeMap and populates it.
        // Cloning Entries is not safe since they hold pointers to other
        // entries in the map, and cloning them just create a new entry
        // but still points to entries in the original map.
        // A proper clone implementation would need additional work
        // to change pointers to point to entries in the new map instead of the original one.
        let mut new_map = AvlTreeMap::new();

        for (k, v) in self.iter() {
            new_map.insert(k.clone(), v.clone());
        }

        new_map
    }
}

impl<K, V> AvlTreeMap<K, V>
where
    K: PartialOrd + Ord,
{
    /// Returns the entry corresponding to the given key, if it is in the tree.
    pub(crate) fn find_key<Q: ?Sized>(&self, key: &Q) -> Option<&Entry<K, V>>
    where
        K: Borrow<Q>,
        Q: PartialOrd,
    {
        let mut tree = self.tree.as_ref().map(|e| unsafe { e.ptr.as_ref() });
        while let Some(entry) = tree {
            let cmp = key.partial_cmp(entry.key().borrow());
            if let Some(cmp) = cmp {
                if cmp == Ordering::Equal {
                    return Some(entry);
                } else if cmp == Ordering::Less {
                    tree = entry.left();
                } else {
                    tree = entry.right();
                }
            }
        }
        tree
    }

    pub(crate) fn find_entry<Q: ?Sized>(&mut self, key: &Q) -> Option<&mut Entry<K, V>>
        where
            K: Borrow<Q>,
            Q: PartialOrd,
    {
        let mut tree = self.tree;
        while let Some(mut entry) = tree {
            let cmp = key.partial_cmp(entry.key().borrow());
            if let Some(cmp) = cmp {
                if cmp == Ordering::Equal {
                    return Some(unsafe { entry.ptr.as_mut() });
                } else if cmp == Ordering::Less {
                    tree = entry.raw_left();
                } else {
                    tree = entry.raw_right();
                }
            }
        }
        unsafe { tree.map(|mut p| p.ptr.as_mut()) }
    }

    /// Locates a key, if the key is present, than the entry for the key is returned,
    /// otherwise, it will be either the smallest greater key or the greatest smaller key.
    #[allow(dead_code)]
    fn locate_key(&self, key: &K) -> Option<&Entry<K, V>> {
        let mut last = self.tree.as_ref().map(|e| unsafe { e.ptr.as_ref() });
        let mut tree = self.tree.as_ref().map(|e| unsafe { e.ptr.as_ref() });
        let mut cmp = None;
        while let Some(entry) = tree {
            cmp = key.partial_cmp(entry.key());
            if let Some(cmp) = cmp {
                if cmp == Ordering::Equal {
                    return Some(entry);
                } else if cmp == Ordering::Less {
                    last = tree;
                    tree = entry.left();
                } else {
                    last = tree;
                    tree = entry.right();
                }
            }
        }

        if cmp == Some(Ordering::Equal) {
            tree
        } else {
            last
        }
    }

    #[allow(dead_code)]
    fn reset_insert_path(&mut self) {
        for i in 0..self.insert_path.len() {
            self.insert_path[i] = false;
        }
    }

    pub fn contains_value<Q: ?Sized>(&self, value: &Q) -> bool
    where
        V: Borrow<Q>,
        Q: PartialEq,
    {
        let mut iter = self.iter();
        while let Some(n) = iter.next() {
            if value.eq(n.1.borrow()) {
                return true;
            }
        }

        false
    }

    pub fn contains_key<Q: ?Sized>(&self, key: &Q) -> bool
    where
        K: Borrow<Q>,
        Q: PartialOrd,
    {
        self.find_key(key).is_some()
    }

    pub fn get<Q: ?Sized>(&self, key: &Q) -> Option<&V>
        where
            K: Borrow<Q>,
            Q: PartialOrd,
    {
        self.find_key(key).map(|v| v.value())
    }

    pub fn insert(&mut self, key: K, value: V) -> Option<V> {
        //let mut modified = false;
        let e;

        if let None = self.tree {
            let entry = Entry::new(key, value); // TODO: Avoid cloning
            let entry = Box::new(entry);
            let ptr = EntryPtr::from_box(entry);
            self.first = Some(ptr);
            self.last = self.first;
            self.tree = Some(ptr);
            self.count += 1;
            return None;
        } else {
            let mut p = self.tree;
            let mut y = p;
            let mut q = None;
            let mut z = None;
            let w;
            let mut cmp;
            let mut i = 0usize;

            // Carefuly maintain this loop to never end up with an infinite loop
            loop {
                // TODO: Provisory, make p and y meet the borrow checker rules
                //cmp = *(&key.cmp(p.key()));
                cmp = *(&key.cmp(p.unwrap().key()));
                if cmp == Ordering::Equal {
                    return Some(p.unwrap().set_value(value));
                }

                // p.balance()
                if p.unwrap().balance() != 0 {
                    i = 0;
                    z = q;
                    y = p;
                }

                let greater = cmp == Ordering::Greater;
                self.insert_path[i] = greater;

                i += 1;

                if greater {
                    // p.succ()
                    if p.unwrap().succ() {
                        self.count += 1;
                        let entry = Entry::new(key, value); // TODO: avoid cloning
                        let entry = Box::new(entry);
                        let raw_entry = Box::into_raw(entry);
                        let mut entry_ptr = EntryPtr::new(NonNull::new(raw_entry).unwrap());
                        e = Some(entry_ptr);
                        //modified = true;

                        if let None = p.unwrap().right_field() {
                            self.last = Some(entry_ptr);
                        }

                        let mut raw_p = p.unwrap();

                        entry_ptr.set_left_field(Some(raw_p));

                        entry_ptr.set_right_field(raw_p.right_field());
                        raw_p.set_right_entry(Some(entry_ptr));
                        break;
                    }

                    q = p;
                    p = p.unwrap().right_field(); // What we do if unwrap() fails?
                                                  //p = NonNull::new(unsafe { p.as_ref() }.raw_right()).unwrap();
                } else {
                    if p.unwrap().pred() {
                        self.count += 1;
                        let entry = Entry::new(key, value); // TODO: avoid cloning
                        let entry = Box::new(entry);
                        let raw_entry = Box::into_raw(entry);
                        let mut entry_ptr = EntryPtr::new(NonNull::new(raw_entry).unwrap());
                        e = Some(entry_ptr);
                        //modified = true;

                        if let None = p.unwrap().left_field() {
                            self.first = Some(entry_ptr);
                        }

                        let mut raw_p = p.unwrap();

                        entry_ptr.set_right_field(p);
                        entry_ptr.set_left_field(raw_p.left_field());
                        raw_p.set_left_entry(Some(entry_ptr));
                        break;
                    }

                    q = p;
                    p = p.unwrap().left_field(); // What we do if unwrap() fails?
                }
            }
            p = y;
            i = 0;
            while let Some(c_entry) = e {
                if p.is_none() || p.unwrap().as_ptr() == c_entry.as_ptr() {
                    break;
                }

                if self.insert_path[i] {
                    p.unwrap().increment_balance();
                } else {
                    p.unwrap().decrement_balance();
                }

                if self.insert_path[i] {
                    p = p.unwrap().right_field();
                    if p.is_none() {
                        break;
                    }
                } else {
                    p = p.unwrap().left_field();
                    if p.is_none() {
                        break;
                    }
                }

                i += 1;
            }

            // Balance
            if y.unwrap().balance() == -2 {
                // x = y.left
                let mut x = y.unwrap().left_field().unwrap();

                let mut y = y.unwrap();

                if x.balance() == -1 {
                    // Ok(1)
                    w = Some(x);

                    if x.succ() {
                        x.set_succ(false);
                        y.set_pred_entry(Some(x));
                    } else {
                        y.set_left_field(x.right_field());
                    }

                    x.set_right_field(Some(y)); // Rotate
                    x.set_balance(0);
                    y.set_balance(0);
                } else {
                    debug_assert!(x.balance() == 1);
                    w = x.raw_right(); // w = x.right
                    let mut w = w.unwrap();

                    {
                        // Rotate
                        x.set_right_field(w.left_field()); //x.right = w.left | x.right = x.right.left | sets the right side of X as left side of X.R
                        w.set_left_field(Some(x)); // w.left = x | sets the left side of the right side of X as the X
                        y.set_left_field(w.right_field()); // y.left = w.right
                        w.set_right_field(Some(y));
                        // w.right = y
                    }

                    if w.balance() == -1 {
                        x.set_balance(0);
                        y.set_balance(1);
                    } else if w.balance() == 0 {
                        x.set_balance(0);
                        y.set_balance(0);
                    } else {
                        x.set_balance(-1);
                        y.set_balance(0);
                    }

                    w.set_balance(0);

                    if w.pred() {
                        // Rotate
                        x.set_succ_entry(Some(w)); // x.succ(w)
                        w.set_pred(false);
                    }

                    if w.succ() {
                        // Rotate
                        y.set_pred_entry(Some(w)); // y.pred(w)
                        w.set_succ(false);
                    }
                }
            } else if y.unwrap().balance() == 2 {
                let mut y = y.unwrap();
                let mut x = y.right_field().unwrap(); // x = y.right

                if x.balance() == 1 {
                    // Rotate
                    w = Some(x);

                    if x.pred() {
                        x.set_pred(false);
                        y.set_succ_entry(Some(x)); // y.succ(x)
                    } else {
                        y.set_right_field(x.left_field());
                        // y.right = x.left
                    }

                    x.set_left_field(Some(y)); // x.left = y
                    x.set_balance(0);
                    y.set_balance(0);
                } else {
                    debug_assert!(x.balance() == -1);
                    w = x.left_field(); // w = x.left
                    let mut w = w.unwrap();
                    {
                        // Rotate
                        x.set_left_field(w.right_field()); // x.left = w.right
                        w.set_right_field(Some(x)); // w.right = x
                        y.set_right_field(w.left_field()); // y.right = w.left
                        w.set_left_field(Some(y));
                        // w.left = y
                    }

                    if w.balance() == 1 {
                        x.set_balance(0);
                        y.set_balance(-1);
                    } else if w.balance() == 0 {
                        x.set_balance(0);
                        y.set_balance(0);
                    } else {
                        x.set_balance(1);
                        y.set_balance(0);
                    }

                    w.set_balance(0);

                    if w.pred() {
                        y.set_succ_entry(Some(w)); // y.pred(w)
                        w.set_pred(false);
                    }

                    if w.succ() {
                        x.set_pred_entry(Some(w)); // x.pred(w)
                        w.set_succ(false);
                    }
                }
            } else {
                return None;
            };

            if z.is_none() {
                self.tree = w;
            } else {
                let mut z = z.unwrap();
                if z.raw_left().map(|l| l.as_ptr()) == y.map(|y| y.as_ptr()) {
                    z.set_left_field(w);
                } else {
                    z.set_right_field(w);
                }
            }
        }

        None
    }

    pub fn retain<F>(&mut self, mut f: F)
        where
            F: FnMut(&K, &mut V) -> bool,
    {
        for mut entry in self.entry_iter() {
            let (key, value) = entry.as_mut();
            if !f(key, value) {
                self.remove(key);
            }
        }
    }

    pub fn remove<Q: ?Sized>(&mut self, key: &Q) -> Option<V> where K: Borrow<Q>, Q: PartialOrd + Ord {
        self.remove_key(key).map(|v| v.take())
    }

    pub(crate) fn remove_key<Q: ?Sized>(&mut self, key: &Q) -> Option<Entry<K, V>> where K: Borrow<Q>, Q: PartialOrd + Ord {
        self.modified = false;
        let mut found_entry = self.tree?;
        let mut cmp;
        let mut parent_entry = None;
        let mut greater = false;

        let kk = key; // Inline

        loop {
            cmp = kk.cmp(found_entry.key().borrow());
            if cmp == Ordering::Equal {
                break;
            }
            greater = cmp == Ordering::Greater;

            if greater {
                // if greater
                parent_entry = Some(found_entry);
                found_entry = found_entry.right_field()?;
            } else {
                parent_entry = Some(found_entry);
                found_entry = found_entry.left_field()?;
            }
        }
        // Loop end

        self.remove_entry(parent_entry, found_entry, greater)
    }

    #[allow(dead_code)]
    pub(crate) fn remove_entry_<Q: ?Sized>(&mut self, prev: Option<EntryPtr<K, V>>, entry: EntryPtr<K, V>) -> Option<Entry<K, V>> where K: Borrow<Q>, Q: PartialOrd + Ord {
        let mut greater = false;
        if let Some(q) = prev {
            greater = entry.key().cmp(q.key()) == Ordering::Greater;
        }

        self.remove_entry(prev, entry, greater)
    }

    pub(crate) fn remove_entry<Q: ?Sized>(&mut self, prev: Option<EntryPtr<K, V>>, entry: EntryPtr<K, V>, mut greater: bool) -> Option<Entry<K, V>> where K: Borrow<Q>, Q: PartialOrd + Ord {
        self.modified = false;
        let mut current_entry = entry;
        let mut previous_entry = prev;

        // Sets first and last nodes
        if current_entry.left_field_ref().is_none() {
            self.first = current_entry.next_as_ptr();
        }

        if current_entry.right_field_ref().is_none() {
            self.last = current_entry.prev_as_ptr();
        }
        // /Sets first and last nodes

        if current_entry.succ() {
            if current_entry.pred() {
                if let Some(mut q) = previous_entry {
                    if greater {
                        q.set_succ_entry(current_entry.right_field());
                    } else {
                        q.set_pred_entry(current_entry.left_field());
                    }
                } else {
                    self.tree = if greater { current_entry.right_field() } else { current_entry.left_field() };
                }
            } else {
                current_entry.prev_as_ptr().unwrap().set_right_entry(current_entry.right_field());

                if let Some(mut q) = previous_entry {
                    if greater {
                        q.set_right_entry(current_entry.left_field())
                    } else {
                        q.set_left_entry(current_entry.left_field())
                    }
                } else {
                    self.tree = current_entry.left_field();
                }
            }
        } else {
            let mut r = current_entry.right_field()?;

            if r.pred() {
                r.set_left_field(current_entry.left_field());
                r.set_pred(current_entry.pred());

                if !r.pred() {
                    r.prev_as_ptr()?.set_right_field(Some(r));
                }

                if let Some(mut q) = previous_entry {
                    if greater {
                        q.set_right_field(Some(r));
                    } else {
                        q.set_left_field(Some(r));
                    }
                } else {
                    self.tree = Some(r);
                }

                r.set_balance(current_entry.balance());
                previous_entry = Some(r);
                greater = true;
            } else {
                let mut s;
                loop {
                    s = r.left_field()?;
                    if s.pred() {
                        break;
                    }
                    r = s;
                }

                if s.succ() {
                    r.set_pred_entry(Some(s));
                } else {
                    r.set_left_entry(s.right_field());
                }

                s.set_left_field(current_entry.left_field());

                if !current_entry.pred() {
                    current_entry.prev_as_ptr()?.set_right_field(Some(s));
                    s.set_pred(false);
                }

                s.set_right_field(current_entry.right_field());
                s.set_succ(true);

                if let Some(mut q) = previous_entry {
                    if greater {
                        q.set_right_field(Some(s));
                    } else {
                        q.set_left_field(Some(s));
                    }
                } else {
                    self.tree = Some(s);
                }

                s.set_balance(current_entry.balance());
                previous_entry = Some(r);
                greater = false;
            }
        }

        let mut y;
        while let Some(q2) = previous_entry {
            y = q2;
            previous_entry = self.parent(y);

            if !greater {
                greater = if let Some(q) = previous_entry {
                    q.left_field_as_ptr() != Some(y.as_ptr())
                } else {
                    false
                };
                y.increment_balance();
                if y.balance() == 1 {
                    break;
                } else if y.balance() == 2 {
                    let mut x = y.right_field().unwrap();
                    if x.balance() == -1 {
                        let mut w = x.left_field().unwrap();
                        x.set_left_field(w.right_field());
                        w.set_right_field(Some(x));
                        y.set_right_field(w.left_field());
                        w.set_left_field(Some(y));

                        if w.balance() == 1 {
                            x.set_balance(0);
                            y.set_balance(-1)
                        } else if w.balance() == 0 {
                            x.set_balance(0);
                            y.set_balance(0)
                        } else {
                            assert_eq!(w.balance(), -1);
                            x.set_balance(1);
                            y.set_balance(0)
                        }

                        w.set_balance(0);
                        if w.pred() {
                            y.set_succ_entry(Some(w));
                            w.set_pred(false);
                        }
                        if w.succ() {
                            x.set_pred_entry(Some(w));
                            w.set_succ(false);
                        }

                        if let Some(mut q) = previous_entry {
                            if greater {
                                q.set_right_field(Some(w));
                            } else {
                                q.set_left_field(Some(w));
                            }
                        } else {
                            self.tree = Some(w);
                        }
                    } else {
                        if let Some(mut q) = previous_entry {
                            if greater {
                                q.set_right_entry(Some(x));
                            } else {
                                q.set_left_entry(Some(x));
                            }
                        } else {
                            self.tree = Some(x);
                        }

                        if x.balance() == 0 {
                            y.set_right_field(x.left_field());
                            x.set_left_field(Some(y));
                            x.set_balance(-1);
                            y.set_balance(1);
                            break;
                        }
                        assert_eq!(x.balance(), 1);
                        if x.pred() {
                            y.set_succ(true);
                            x.set_pred(false);
                        } else {
                            y.set_right_entry(x.left_field());
                        }
                        x.set_left_field(Some(y));
                        y.set_balance(0);
                        x.set_balance(0);
                    }
                }
            } else {
                greater = if let Some(q) = previous_entry {
                    q.left_field_as_ptr() != Some(y.as_ptr())
                } else {
                    false
                };
                y.decrement_balance();
                if y.balance() == -1 {
                    break;
                } else if y.balance() == -2 {
                    let mut x = y.left_field().unwrap();
                    if x.balance() == 1 {
                        let mut w = x.right_field().unwrap();
                        x.set_right_field(w.left_field());
                        w.set_left_field(Some(x));
                        y.set_left_field(w.right_field());
                        w.set_right_field(Some(y));

                        if w.balance() == -1 {
                            x.set_balance(0);
                            y.set_balance(1);
                        } else if w.balance() == 0 {
                            x.set_balance(0);
                            y.set_balance(0);
                        } else {
                            assert_eq!(w.balance(), 1);
                            x.set_balance(-1);
                            y.set_balance(0);
                        }
                        w.set_balance(0);

                        if w.pred() {
                            x.set_succ_entry(Some(w));
                            w.set_pred(false);
                        }
                        if w.succ() {
                            y.set_pred_entry(Some(w));
                            w.set_succ(false);
                        }
                        if let Some(mut q) = previous_entry {
                            if greater {
                                q.set_right_field(Some(w));
                            } else {
                                q.set_left_field(Some(w));
                            }
                        } else {
                            self.tree = Some(w);
                        }
                    } else {
                        if let Some(mut q) = previous_entry {
                            if greater {
                                q.set_right_field(Some(x));
                            } else {
                                q.set_left_field(Some(x));
                            }
                        } else {
                            self.tree = Some(x);
                        }

                        if x.balance() == 0 {
                            y.set_left_field(x.right_field());
                            x.set_right_field(Some(y));
                            x.set_balance(1);
                            y.set_balance(-1);
                            break;
                        }

                        assert_eq!(x.balance(), -1);

                        if x.succ() {
                            y.set_pred(true);
                            x.set_succ(false);
                        } else {
                            y.set_left_field(x.right_field());
                        }

                        x.set_right_field(Some(y));
                        y.set_balance(0);
                        x.set_balance(0);
                    }
                }
            }
        }

        self.modified = true;
        self.count -= 1;

        if let Some(_) = current_entry.left_field() {
            current_entry.set_left_field(None);
        }

        if let Some(_) = current_entry.right_field() {
            current_entry.set_right_field(None);
        }

        Some(current_entry.take_entry())
    }

    fn parent(&self, entry: EntryPtr<K, V>) -> Option<EntryPtr<K, V>> {
        let _ = self.tree?;
        let mut x;
        let mut y;
        let mut p;
        y = Some(entry);
        x = y;
        loop {
            if y?.succ() {
                p = y?.right_field();
                if p.is_none() || p.unwrap().left_field_as_ptr() != Some(entry.as_ptr()) {
                    while !x?.pred() {
                        x = x?.left_field();
                    }

                    p = x?.left_field();
                }
                return p;
            } else if x?.pred() {
                p = x?.left_field();
                if p.is_none() || p.unwrap().right_field_as_ptr() != Some(entry.as_ptr()) {
                    while !y?.succ() {
                        y = y?.right_field();
                    }
                    p = y?.right_field();
                }
                return p;
            }
            x = x.map(|v| v.left_field()).flatten();
            y = y.map(|v| v.right_field()).flatten();
        }
    }

    pub fn iter(&self) -> AvlTreeIter<'_, K, V> {
        AvlTreeIter::new(self)
    }

    pub(crate) fn entry_iter(&self) -> AvlTreeEntryIter<K, V> {
        AvlTreeEntryIter::new(self)
    }
}

impl<K, V> Drop for AvlTreeMap<K, V> {
    fn drop(&mut self) {
        let mut start = self.tree;
        if let Some(start) = start.take() {
            let _ = unsafe { Box::from_raw(start.as_ptr()) };
        }
    }
}

pub struct AvlTreeIter<'a, K, V> {
    next: Option<EntryPtr<K, V>>,
    prev: Option<EntryPtr<K, V>>,
    _marker: PhantomData<(&'a K, &'a V)>,
}

impl<'a, K, V> AvlTreeIter<'a, K, V> {
    fn new(tree: &'a AvlTreeMap<K, V>) -> Self {
        Self {
            next: tree.first,
            prev: tree.last,
            _marker: PhantomData,
        }
    }
}

impl<'a, K, V> Iterator for AvlTreeIter<'a, K, V>
where
    K: PartialOrd,
{
    type Item = (&'a K, &'a V);

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(e) = self.next {
            if let Some(p) = self.prev {
                if e.key().partial_cmp(p.key()) == Some(Ordering::Greater) {
                    return None;
                }
            }

            let key = unsafe { (*e.ptr.as_ptr()).key() };
            let value = unsafe { (*e.ptr.as_ptr()).value() };
            let kv = (key, value);
            self.next = e.next_as_ptr();
            Some(kv)
        } else {
            None
        }
    }
}

impl<'a, K, V> DoubleEndedIterator for AvlTreeIter<'a, K, V>
where
    K: PartialOrd,
{
    fn next_back(&mut self) -> Option<Self::Item> {
        if let Some(e) = self.prev {
            if let Some(p) = self.next {
                if e.key().partial_cmp(p.key()) == Some(Ordering::Less) {
                    return None;
                }
            }

            let key = unsafe { (*e.ptr.as_ptr()).key() };
            let value = unsafe { (*e.ptr.as_ptr()).value() };
            let kv = (key, value);
            self.prev = e.prev_as_ptr();
            Some(kv)
        } else {
            None
        }
    }
}

pub(crate) struct AvlTreeEntryIter<K, V> {
    next: Option<EntryPtr<K, V>>,
    prev: Option<EntryPtr<K, V>>,
}

impl<K, V> AvlTreeEntryIter<K, V> {
    fn new(tree: &AvlTreeMap<K, V>) -> Self {
        Self {
            next: tree.first,
            prev: tree.last
        }
    }
}

impl<K, V> Iterator for AvlTreeEntryIter<K, V>
    where
        K: PartialOrd,
{
    type Item = EntryPtr<K, V>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(e) = self.next {
            if let Some(p) = self.prev {
                if e.key().partial_cmp(p.key()) == Some(Ordering::Greater) {
                    return None;
                }
            }

            self.next = e.next_as_ptr();
            Some(e)
        } else {
            None
        }
    }
}

impl<K, V> DoubleEndedIterator for AvlTreeEntryIter<K, V>
    where
        K: PartialOrd,
{
    fn next_back(&mut self) -> Option<Self::Item> {
        if let Some(e) = self.prev {
            if let Some(p) = self.next {
                if e.key().partial_cmp(p.key()) == Some(Ordering::Less) {
                    return None;
                }
            }

            self.prev = e.prev_as_ptr();
            Some(e)
        } else {
            None
        }
    }
}


#[cfg(not(feature = "print-tree"))]
impl<K, V> Debug for AvlTreeMap<K, V>
where
    K: Debug + Ord,
    V: Debug,
{
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        let mut d = f.debug_map();

        for (k, v) in self.iter() {
            d.entry(k, v);
        }

        d.finish()
    }
}

#[cfg(feature = "print-tree")]
impl<K, V> Debug for AvlTreeMap<K, V>
where
    K: Debug,
    V: Debug,
{
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "AVLTree {{\n")?;

        let current_node = self.tree;
        if let Some(current_node) = current_node {
            let tree = TreeDebug {
                root: current_node.ptr,
            };
            write!(f, "{:?}", tree)?;
        }
        write!(f, "}}")
    }
}

#[cfg(feature = "print-tree")]
struct TreeDebug<K, V> {
    root: NonNull<Entry<K, V>>,
}

#[cfg(feature = "print-tree")]
impl<K, V> Debug for TreeDebug<K, V>
where
    K: Debug,
    V: Debug,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let root_node = EntryPtr::new(self.root);

        fn write_child<K, V>(
            f: &mut Formatter<'_>,
            node: EntryPtr<K, V>,
            pref: &str,
            child_pref: &str,
            postf: &str,
        ) -> std::fmt::Result
        where
            K: Debug,
            V: Debug,
        {
            write!(f, "{}{:?}{}\n", pref, unsafe { node.ptr.as_ref() }, postf)?;
            let left = node.raw_left();
            let right = node.raw_right();
            let has_right = right.is_some();

            if let Some(left) = left {
                if has_right {
                    write_child(
                        f,
                        left,
                        &format!("{}├── L(", child_pref),
                        &format!("{}│   ", child_pref),
                        ")",
                    )?;
                } else {
                    write_child(
                        f,
                        left,
                        &format!("{}└── L(", child_pref),
                        &format!("{}    ", child_pref),
                        ")",
                    )?;
                }
            }

            if let Some(right) = right {
                write_child(
                    f,
                    right,
                    &format!("{}└── R(", child_pref),
                    &format!("{}    ", child_pref),
                    ")",
                )?;
            }

            Ok(())
        }

        write_child(f, root_node, "    Root(", "    ", ")")
    }
}
